package servlet;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/ex1")
public class EX1 extends HttpServlet {
	protected void doGet(HttpServletRequest request,
			HttpServletResponce responce)
			throws ServletException, IOException{

	//UTF-8のHTMLをレスポンス
	responce.setContentType("text/html;charset=UTF-8");
	PrintWriter out = responce.getWriter();
	out.println("<html><body>Hello</body></html>");
	}
