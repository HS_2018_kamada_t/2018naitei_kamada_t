・ブラウザの動作
コード「text/html;charset=UTF-8」を「ABCDE」に修正してブラウザから実行すると、多くのブラウザはサーバから送られてきたHTML文字列を画面に表示せず、ファイルとして保存しようと動作する。
・動作の理由
setContentTypeの部分が正しく「text/html;charset=UTF-8」と指定されている場合、WebサーバはWebブラウザに対して以下のようなレスポンスを返す。

HTTP/1.1  200  OK
Content-Type: text/html; charset-UTF-8
　・
　・
<html><body>Hello </body></html>

ブラウザは、Content-Typeヘッダを見て、レスポンスボディ部の情報がHTMLであると理解し、HTMLコードを正しく画面に表示する。しかしContentTypeの部分を変更すると、ブラウザに返されるHTTPレスポンス中のContent-Typeヘッダも「ABCDE」という不正な値に変化する。ブラウザは、Content-Typeヘッダを見ても、レスポンスボディ部の情報がHTML/画像/音声/PDFのいずれか、区別できない。画面に表示できない可能性も考慮し、多くのブラウザでは「処理方法が不明なコンテンツを受信したら、レスポンスボディをとりあえずファイルとして保存する」という動作を採用していると考えられる。