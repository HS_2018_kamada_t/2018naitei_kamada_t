

import java.io.IOException;

@WebServlet("/ex62servlet")
public class ex62servlet extends HttpServlet {
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		int rand = (int) (Math.random() * 10);
		if(rand % 2 == 1){
			response.sendRedirect("/ex62/redirected.jsp");
		}else{
		 RequestDispatcher d = request.getRequestDispatcher("/forwarded.jsp");
		 d.forward(request,response);
		}
	}

}
